project(
    WAT
    LANGUAGES CXX
    VERSION 6.4.0.0
    DESCRIPTION "cWB pipeline source library"
)

#Include the master CMakeLists.txt
include_directories(${CMAKE_CURRENT_SOURCE_DIR})

# Comands to get the git repository properties
execute_process(COMMAND git -C ${CMAKE_CURRENT_SOURCE_DIR} rev-parse HEAD  OUTPUT_VARIABLE GIT_HASH OUTPUT_STRIP_TRAILING_WHITESPACE)
execute_process(COMMAND git -C ${CMAKE_CURRENT_SOURCE_DIR} rev-parse --short HEAD  OUTPUT_VARIABLE GIT_HASH_SHORT OUTPUT_STRIP_TRAILING_WHITESPACE)
execute_process(COMMAND bash -c " git -C ${CMAKE_CURRENT_SOURCE_DIR} branch | grep \\* | cut -d ' ' -f2"  OUTPUT_VARIABLE GIT_BRANCH OUTPUT_STRIP_TRAILING_WHITESPACE)
execute_process(COMMAND  bash -c " git -C ${CMAKE_CURRENT_SOURCE_DIR} show | head | grep Date" OUTPUT_VARIABLE GIT_DATE OUTPUT_STRIP_TRAILING_WHITESPACE)
execute_process(COMMAND git -C ${CMAKE_CURRENT_SOURCE_DIR} config --get remote.origin.url  OUTPUT_VARIABLE GIT_REMOTE_ORIGIN OUTPUT_STRIP_TRAILING_WHITESPACE)
execute_process(COMMAND git -C ${CMAKE_CURRENT_SOURCE_DIR} tag -l --points-at HEAD  OUTPUT_VARIABLE GIT_TAG OUTPUT_STRIP_TRAILING_WHITESPACE)

#Get information about build system and build time
execute_process(COMMAND uname -v OUTPUT_VARIABLE UNAME_VERSION OUTPUT_STRIP_TRAILING_WHITESPACE)
execute_process(COMMAND uname -m OUTPUT_VARIABLE UNAME_MACH OUTPUT_STRIP_TRAILING_WHITESPACE)
execute_process(COMMAND date -u OUTPUT_VARIABLE BTIME_UTC OUTPUT_STRIP_TRAILING_WHITESPACE)
string(TIMESTAMP CMAKE_BTIME_UTC "%Y-%m-%d %I-%M-%S" UTC)


#Configure the files from the varibles already set
configure_file(watversion.hh.in watversion.hh)
configure_file(wathash.in wathash)
configure_file(wattag.in wattag)
configure_file(watbranch.in watbranch)
configure_file(waturl.in waturl)

#Add an append of the current binary directory so the above configured files can be found and add it to the parent scope
list(APPEND CWB_INCLUDE_DIRS ${CMAKE_CURRENT_BINARY_DIR} ${CMAKE_CURRENT_SOURCE_DIR})
set(CWB_INCLUDE_DIRS ${CWB_INCLUDE_DIRS} PARENT_SCOPE)


set(WAT_SRC injection.cc wavecomplex.cc Wavelet.cc WaveDWT.cc Haar.cc Biorthogonal.cc Daubechies.cc Symlet.cc Meyer.cc SymmArray.cc SymmArraySSE.cc SymmObjArray.cc WDM.cc wavearray.cc wseries.cc watplot.cc cluster.cc wavecor.cc wavefft.cc waverdc.cc lossy.cc wavelinefilter.cc netpixel.cc netcluster.cc skymap.cc detector.cc network.cc netevent.cc regression.cc time.cc monster.cc sseries.cc)

#Get all the header files
string(REGEX REPLACE "[.]cc" ".hh" WAT_HDRS "${WAT_SRC}")

#Include the header file that
if(HEALPIX_FOUND)
   list(APPEND WAT_HDRS alm.hh)
endif()

#include all the created header files
list(APPEND WAT_HDRS watversion.hh watfun.hh constants.hh)

#Add the watasm_elf64.o to the list of src
list(APPEND WAT_SRC watasm_elf64.o)

#Create the library
add_library(${PROJECT_NAME} SHARED ${WAT_SRC})

#Set the target properties
set_target_properties(${PROJECT_NAME} PROPERTIES PREFIX "")
set_target_properties(${PROJECT_NAME} PROPERTIES OUTPUT_NAME "wavelet-${XIFO}x")
set_target_properties(${PROJECT_NAME} PROPERTIES LIBRARY_OUTPUT_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/lib)

#Add all the include directories to the target
target_include_directories(${PROJECT_NAME} PRIVATE ${CWB_INCLUDE_DIRS})

target_link_libraries(${PROJECT_NAME} ROOT::TreePlayer ROOT::Core ROOT::Graf ROOT::FFTW ROOT::Tree ROOT::Gpad ROOT::Physics ${HEALPIX_LIBRARY})

#Generate the root dictionary
ROOT_GENERATE_DICTIONARY(wavedict ${WAT_HDRS}
    LINKDEF wavelet_LinkDef.h
    MODULE ${PROJECT_NAME})

#Create system links to other possible names of the library
add_custom_command(
    TARGET ${PROJECT_NAME} POST_BUILD
    COMMAND ln -sf wavelet-${XIFO}x.so wavelet.so
    COMMAND ln -sf wavelet.so libwavelet.so
    WORKING_DIRECTORY lib
)

install(FILES ${CMAKE_CURRENT_BINARY_DIR}/lib/wavelet-${XIFO}x.so 
              ${CMAKE_CURRENT_BINARY_DIR}/lib/wavelet.so
              ${CMAKE_CURRENT_BINARY_DIR}/lib/libwavelet.so
              ${CMAKE_CURRENT_BINARY_DIR}/${CMAKE_SHARED_LIBRARY_PREFIX}${PROJECT_NAME}_rdict.pcm
        TYPE LIB)
