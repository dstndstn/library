project(
    BICO
    LANGUAGES CXX
)

#Include the master CMakeLists.txt
include_directories(${CMAKE_CURRENT_SOURCE_DIR})

set(BICO_SRC Bicoherence.cc)

#Get all the header files
string(REGEX REPLACE "[.]cc" ".hh" BICO_HDRS "${BICO_SRC}")

#Make a local copy of the CWB_OPTS
set(BICO_OPTS ${CWB_OPTS})

#Define the Framelib version
if(DEFINED FRAMELIB_VERSION)
    list(APPEND BICO_OPTS "-DFRAMELIB_VERSION=${FRAMELIB_VERSION}")
endif()

#Make a local copy of the CWB_INCLUDE_DIRS
set(BICO_INCLUDE_DIRS ${CWB_INCLUDE_DIRS})

#Append the stft,toolbox and history directories
list(APPEND BICO_INCLUDE_DIRS ${CMAKE_CURRENT_LIST_DIR}/../stft ${CMAKE_CURRENT_LIST_DIR}/../toolbox ${CMAKE_CURRENT_LIST_DIR}/../history)

#Create the library
add_library(${PROJECT_NAME} SHARED ${BICO_SRC})


#Set the target properties
set_target_properties(${PROJECT_NAME} PROPERTIES PREFIX "")
set_target_properties(${PROJECT_NAME} PROPERTIES OUTPUT_NAME "Bicoherence")
set_target_properties(${PROJECT_NAME} PROPERTIES LIBRARY_OUTPUT_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/lib)

#Add all the include directories to the target
target_include_directories(${PROJECT_NAME} PRIVATE ${BICO_INCLUDE_DIRS})

target_link_libraries(${PROJECT_NAME} PUBLIC WAT STFT TOOLBOX ROOT::Core ROOT::Hist ROOT::Gpad)

#Generate the root dictionary
ROOT_GENERATE_DICTIONARY(Bicoherence_dict ${BICO_HDRS}
    LINKDEF Bicoherence_LinkDef.h
    MODULE ${PROJECT_NAME})

install(TARGETS ${PROJECT_NAME})

install(FILES ${CMAKE_CURRENT_BINARY_DIR}/${CMAKE_SHARED_LIBRARY_PREFIX}${PROJECT_NAME}_rdict.pcm
        TYPE LIB)
