project(
    HISTORY
    LANGUAGES CXX
)

#Include the master CMakeLists.txt
include_directories(${CMAKE_CURRENT_SOURCE_DIR})

set(HISTORY_SRC History.cc HistoryLine.cc  HistoryLogLine.cc	HistoryStage.cc)

#Get all the header files
string(REGEX REPLACE "[.]cc" ".hh" HISTORY_HDRS "${HISTORY_SRC}")

#Create the library
add_library(${PROJECT_NAME} SHARED ${HISTORY_SRC})

#Set the target properties
set_target_properties(${PROJECT_NAME} PROPERTIES PREFIX "")
set_target_properties(${PROJECT_NAME} PROPERTIES OUTPUT_NAME "History")
set_target_properties(${PROJECT_NAME} PROPERTIES LIBRARY_OUTPUT_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/lib)

#Add all the include directories to the target
target_include_directories(${PROJECT_NAME} PRIVATE ${CWB_INCLUDE_DIRS})

target_link_libraries(${PROJECT_NAME} PUBLIC ROOT::Core ROOT::MathCore ROOT::RIO)

#Generate the root dictionary
ROOT_GENERATE_DICTIONARY(History_dict ${HISTORY_HDRS}
    LINKDEF History_LinkDef.h
    MODULE ${PROJECT_NAME})


install(TARGETS ${PROJECT_NAME})

install(FILES ${CMAKE_CURRENT_BINARY_DIR}/${CMAKE_SHARED_LIBRARY_PREFIX}${PROJECT_NAME}_rdict.pcm
        TYPE LIB)
