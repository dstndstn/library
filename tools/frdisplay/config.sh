#!/bin/bash

if [[  "$OSTYPE" =~ "linux"  ]]; then
  export BAUDLINE_CMD="${HOME_BAUDLINE}/baudline"
fi 
if [[  "$OSTYPE" =~ "darwin"  ]]; then
  export BAUDLINE_CMD="${HOME_BAUDLINE}/baudline.app/Contents/MacOS/baudline"
fi 
export BAUDLINE_PALETTE="${HOME_BAUDLINE}/palettes/auriga.palette"

export FRDISPLAY_DIR=${HOME_FRDISPLAY}
export FRDISPLAY_WORKDIR="."
export FRDISPLAY_WORKSITE=${SITE_CLUSTER}

alias baudline=$BAUDLINE_CMD

alias FrDisplay="${FRDISPLAY_DIR}/FrDisplay "
alias FrDisplayPROC="${FRDISPLAY_DIR}/FrDisplay -d 5 -proc "
alias FrDisplayADC="${FRDISPLAY_DIR}/FrDisplay -d 5 -adc "

alias kbaudline='ps | grep baudline | awk '"'"'{print $1}'"'"' | xargs kill -9'
alias kFrDisplay='ps | grep FrDisplay | awk '"'"'{print $1}'"'"' | xargs kill -9'
