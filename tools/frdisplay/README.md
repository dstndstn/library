# Data Display

The **FrDisplay** tool enables the plotting of spectrograms, spectra and
waveforms from data contained in frame files. It wraps the considered
frame files and [Baudline](http://www.baudline.com/), 
a software developed for data visualization in the time-frequency domain. FrDisplay can
also be used to list the channels available in the considered frames,
see [cWB online manual](https://gwburst.gitlab.io/documentation/latest/html/frdisplay.html).

