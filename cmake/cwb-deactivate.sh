#!/usr/bin/env bash

# reinstate the backup from outside the environment
if [ ! -z "${BACKUP_CWB_ROOTLOGON_FILE}" ]; then
	export CWB_ROOTLOGON_FILE="${BACKUP_CWB_ROOTLOGON_FILE}"
	unset BACKUP_CWB_ROOTLOGON_FILE
# no backup, just unset
else
	unset CWB_ROOTLOGON_FILE
fi