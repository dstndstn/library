#!/bin/sh

set -e  # fail on errors
set -x  # print commands as they are executed (xtrace)

# update watversion.hh
cd wat
make watversion
cd ..

# use fresh build directory
rm -rf build
mkdir -p build
cd build

# detect ICC
if ! [ -x "$(command -v icc)" ]; then
  echo 'Intel compiler is not installed. Reverting to gcc..' >&2
else
  echo 'Compiling with icc ... '
  CMAKE_ARGS="-DCMAKE_CXX_COMPILER=icc ${CMAKE_ARGS}"
fi

# configure
cmake .. \
    ${CMAKE_ARGS} \
    -DCMAKE_INSTALL_PREFIX:PATH="../tools/install"

# build
cmake --build . --verbose --parallel ${CPU_COUNT}

# install
cmake --build . --verbose --parallel ${CPU_COUNT} --target install

# return
cd ..
