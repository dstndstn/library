<div id="top"></div>
<!--
*** Thanks for checking out the Best-README-Template. If you have a suggestion
*** that would make this better, please fork the repo and create a pull request
*** or simply open an issue with the tag "enhancement".
*** Don't forget to give the project a star!
*** Thanks again! Now go create something AMAZING! :D
-->



<!-- PROJECT SHIELDS -->
<!--
*** I'm using markdown "reference style" links for readability.
*** Reference links are enclosed in brackets [ ] instead of parentheses ( ).
*** See the bottom of this document for the declaration of the reference variables
*** for contributors-url, forks-url, etc. This is an optional, concise syntax you may use.
*** https://www.markdownguide.org/basic-syntax/#reference-style-links
-->
[![Contributors][contributors-shield]][contributors-url]
[![Issues][issues-shield]][issues-url]
[![GPLv3 License][license-shield]][license-url]
<!--
[![Forks][forks-shield]][forks-url]
[![Stargazers][stars-shield]][stars-url]
-->

<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://gitlab.com/gwburst/public/library">
    <img src="tools/cwb/www/logo/cwb_logo_icon_modern.png" alt="Logo" width="80" height="80">
  </a>

<h3 align="center">cWB public libraries</h3>

  <p align="center">
    An open source software for gravitational-wave data analysis
    <br />
    <a href="https://gwburst.gitlab.io/"><strong>cWB Home page »</strong></a>
    <br />
    <br />
    <a href="https://gitlab.com/gwburst/public/library/merge_requests">Merge Requests</a>
    ·
    <a href="https://gitlab.com/gwburst/public/library/issues">Report Bug</a>
    ·
    <a href="https://gitlab.com/gwburst/public/library/issues">Request Feature</a>
  </p>
</div>



<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#acknowledgments">Acknowledgments</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project

[![Product Name Screen Shot][product-screenshot]](https://gwburst.gitlab.io/)



<p align="right">(<a href="#top">back to top</a>)</p>



## Getting Started

In order to set up your cWB library locally, you may follow the [cWB documentation](https://gwburst.gitlab.io/documentation/latest/html/install/cwb.html#cwb-library-installation) or, alternatively,
you may try using the [build.sh script](build.sh).

### Prerequisites


* root, an object oriented framework for large scale data analysis; 
* lal libraries
  - lalsimulation
  - lalburst
  - lalinspiral
  - libmetaio

* HEALPix, an acronym for Hierarchical Equal Area isoLatitude Pixelization of a sphere
* CFITSIO, a library of C and Fortran subroutines for reading and writing data files in FITS (Flexible Image Transport System) data format
* LibFramel (Frame Library), a dedicated software for frame manipulation including file input/output
* CVODE (used by eBBH) solves initial value problems for ordinary differential equation (ODE) systems
 
### Installation

1. Clone the repo
   ```sh
   git clone https://gitlab.com/gwburst/public/library.git
   ```
2. ```sh

   cd library
   ```
3. Compile and install it locally
   ```sh
   bash -ex build.sh
   ```
4. Activate cWB environment
   ```sh 
   source source tools/install/etc/cwb/cwb-activate.sh
   ```
<p align="right">(<a href="#top">back to top</a>)</p>



<!-- USAGE EXAMPLES -->
## Test the installation

The easiest way to test your locally installed cWB is to re-analyze LVK public data released within the GWOSC and containing GW events.

Such analysis can be easily performed with a dedicated cWB command: cwb_gwosc.

The command line to execute the full analysis of GW150914 event is:
```sh
cwb_gwosc GW=GW150914 all
```

The output of the previous command is the Coherent Event Display ( CED ) of the GW150914 event, i.e. an html report that can be visualized with a browser

```sh
cwb_gwosc GW=GW150914 xced
```

_For more examples, please refer to the [Documentation](https://gwburst.gitlab.io/documentation/latest/html/index.html)_

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- ROADMAP -->
<!--
## Roadmap

- [] Feature 1
- [] Feature 2
- [] Feature 3
    - [] Nested Feature
-->
See the [open issues](https://gitlab.com/gwburst/public/library/issues) for a full list of known issues (and proposed features).

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- CONTRIBUTING -->
## Contributing

Contributions are what make the open source community such an amazing place to learn, inspire, and create. Any contributions you make are **greatly appreciated**.

If you have a suggestion that would make this better, please fork the repo and create a pull request. You can also simply open an issue with the tag "enhancement".
Don't forget to give the project a star! Thanks again!

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- LICENSE -->
## License

Distributed under the GPLv3 License. See `LICENSE.txt` for more information.

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- CONTACT -->
<!--
## Contact

Your Name - [@twitter_handle](https://twitter.com/twitter_handle) - email@email_client.com

Project Link: []()

<p align="right">(<a href="#top">back to top</a>)</p>

-->

<!-- ACKNOWLEDGMENTS -->
## Use of cWB in scientific publications


Please refer to [citation page](https://gwburst.gitlab.io/info/citations/) on our web site for more details on the citation policy. 

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[contributors-shield]: https://img.shields.io/badge/Contributors-green.svg?style=for-the-badge&logo=gitlab
[contributors-url]: https://gitlab.com/gwburst/public/library/-/graphs/public
[issues-shield]: https://img.shields.io/badge/Issues-orange.svg?style=for-the-badge&logo=gitlab
[issues-url]: https://gitlab.com/gwburst/public/library/issues
[license-shield]: https://img.shields.io/badge/License-green.svg?style=for-the-badge&logo=gitlab
[license-url]: https://gitlab.com/gwburst/public/library/blob/public/LICENSE
[product-screenshot]: https://gwburst.gitlab.io/img/gwtc-1-2.RED.png


<!--
[forks-shield]: https://img.shields.io/github/forks/github_username/repo_name.svg?style=for-the-badge
[forks-url]: https://gitlab.com/gwburst/public/library/network/members
[stars-shield]: https://img.shields.io/github/stars/github_username/repo_name.svg?style=for-the-badge
[stars-url]: https://gitlab.com/gwburst/public/library/stargazers
--->

